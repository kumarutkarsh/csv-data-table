import styled from "@emotion/styled";
import { TableCell, Table } from "@mui/material";

export const StyledTableCell = styled(TableCell)`
  font-weight: bold!important;
  font-size: 1rem!important;
`;

export const ModalContainer = styled.div`
  background-color: #fff;
  padding: 2rem;
  width: 60%;
  margin: auto;
  margin-top: 10vh;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.2);
  max-height: 70vh;
  overflow: auto;
`;

export const AppContainer = styled.div`
  padding: 3rem;
  background-color: #f5f5f5;
  min-height: 100vh;
`;

export const DataTable = styled(Table)`
  min-width: 800px;
  border: 1px solid #ddd;
`

export const ModalTableCell = styled(TableCell)`
  font-size: 0.9rem;
`