export const downloadUpdatedCSV = (csvData, updatedData) => {
  const updatedCSV = csvData
    .map((row, rowIndex) => {
      if (rowIndex === 0) {
        // Include the header row
        return row.join(",");
      } else {
        // using 1st and 5th columns as key coz most of table have the same part#
        const keyColumnIndex1 = 0;
        const keyColumnIndex2 = 4;
        const key1 = row[keyColumnIndex1];
        const key2 = row[keyColumnIndex2];
        const updatedRow = updatedData.find(
          (updatedRow) =>
            updatedRow[keyColumnIndex1] === key1 &&
            updatedRow[keyColumnIndex2] === key2
        );

        if (updatedRow) {
          // Use updated row if available
          return updatedRow.join(",");
        } else {
          // Use original row if not updated
          return row.join(",");
        }
      }
    })
    .join("\n");

  const blob = new Blob([updatedCSV], { type: "text/csv" });
  const url = URL.createObjectURL(blob);
  const link = document.createElement("a");
  link.href = url;
  link.download = "updated_inventory.csv";
  link.click();
  URL.revokeObjectURL(url);
};
