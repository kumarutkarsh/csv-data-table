import React, { useState, memo } from "react";
import {
  Typography,
  Button,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TextField,
  Grid,
  Modal,
  Fade,
} from "@mui/material";
import { downloadUpdatedCSV } from "./utils";
import { AppContainer, DataTable, ModalContainer, ModalTableCell, StyledTableCell } from "./styledComponents";

const Dashboard = memo(() => {
  const [csvData, setCSVData] = useState([]);
  const [searchString, setSearchString] = useState("");
  const [openModal, setOpenModal] = useState(false);
  const [modalData, setModalData] = useState([]);
  const [updatedData, setUpdatedData] = useState([]);
  const [selectedFileName, setSelectedFileName] = useState("");

  const handleFileUpload = (event) => {
    const file = event.target.files[0];

    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const contents = e.target.result;
        const rows = contents.split("\n");
        const data = rows.map((row) => row.split(","));
        setCSVData(data);
      };
      reader.readAsText(file);
      setSelectedFileName(file.name);
    }
  };

  const handleSearchChange = (event) => {
    setSearchString(event.target.value);
  };

  const handleUpdateInventory = () => {
    setOpenModal(true);
    const filteredData = csvData.slice(1).filter((row) => {
      const partColumn = row[0];
      const altPartColumn = row[1];
      const searchRegex = new RegExp(searchString, "i");
      return searchRegex.test(partColumn) || searchRegex.test(altPartColumn);
    });
    setModalData(filteredData);
    setUpdatedData(filteredData.map((row) => [...row, false])); // Add a flag for each row to track if it's updated
  };

  const handleStockChange = (event, rowIndex, columnIndex) => {
    const updatedStock = [...updatedData];
    updatedStock[rowIndex][columnIndex] = event.target.value;
    updatedStock[rowIndex][updatedStock[rowIndex].length - 1] = true; // Set the flag to true when a value is updated
    setUpdatedData(updatedStock);
  };

  const handleModalClose = () => {
    setOpenModal(false);
    setUpdatedData([]);
  };

  const filteredData = csvData.slice(1).filter((row) => {
    const partColumn = row[0];
    const altPartColumn = row[1];
    const searchRegex = new RegExp(searchString, "i");
    return searchRegex.test(partColumn) || searchRegex.test(altPartColumn);
  });

  const renderTableCell = (cellValue, rowIndex, columnIndex) => {
    if (openModal && (columnIndex === 8 || columnIndex === 10)) {
      return (
        <ModalTableCell key={columnIndex}>
          <TextField
            value={updatedData[rowIndex][columnIndex]}
            onChange={(e) => handleStockChange(e, rowIndex, columnIndex)}
          />
        </ModalTableCell>
      );
    } else {
      return (
        <ModalTableCell key={columnIndex}>
          {cellValue}
        </ModalTableCell>
      );
    }
  };

  return (
    <AppContainer>
      <Typography variant="h6" component="h2" gutterBottom>
        CSV Data Table
      </Typography>

      <input
        type="file"
        accept=".csv"
        id="csv-input"
        style={{ display: "none" }}
        onChange={handleFileUpload}
      />
      <Grid
        container
        alignItems="center"
        spacing={2}
        style={{ marginTop: "1rem" }}
      >
        <Grid item>
          <label htmlFor="csv-input">
            <Button variant="contained" component="span" color="primary">
              Choose File
            </Button>
          </label>
        </Grid>
        <Grid item>
          {selectedFileName && (
            <Typography variant="body1">{selectedFileName}</Typography>
          )}
        </Grid>
      </Grid>

      <Grid
        container
        alignItems="center"
        spacing={2}
        style={{ marginTop: "1rem" }}
      >
        <Grid item>
          <Typography>User Input:</Typography>
        </Grid>
        <Grid item>
          <TextField
            label="Enter search string"
            variant="outlined"
            value={searchString}
            onChange={handleSearchChange}
          />
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            style={{ marginTop: "1rem" }}
            onClick={handleUpdateInventory}
            disabled={csvData.length === 0 || searchString.trim() === ""}
          >
            Update Inventory
          </Button>
        </Grid>
      </Grid>

      {csvData.length > 0 ? (
        <div style={{ overflowX: "auto", marginTop: "1rem" }}>
          <DataTable>
            <TableHead>
              <TableRow>
                {csvData.length > 0 &&
                  csvData[0].map((header, index) => (
                    <StyledTableCell key={index}>{header}</StyledTableCell>
                  ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredData?.map((row, rowIndex) => (
                <TableRow key={rowIndex}>
                  {row.map((cell, cellIndex) =>
                    renderTableCell(cell, rowIndex, cellIndex, openModal)
                  )}
                </TableRow>
              ))}
            </TableBody>
          </DataTable>
        </div>
      ) : (
        <Grid
          container
          alignItems="center"
          spacing={2}
          style={{ marginTop: "1rem" }}
        >
          <Grid item>
            <Typography>No CSV data available.</Typography>{" "}
          </Grid>
        </Grid>
      )}

      <Modal open={openModal} onClose={handleModalClose} closeAfterTransition>
        <Fade in={openModal}>
          <ModalContainer>
            <Table>
              <TableHead>
                <TableRow>
                  {csvData.length > 0 &&
                    csvData[0].map((header, index) => (
                      <TableCell key={index}>{header}</TableCell>
                    ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {modalData.map((row, rowIndex) => (
                  <TableRow key={rowIndex}>
                    {row.map((cell, cellIndex) =>
                      renderTableCell(cell, rowIndex, cellIndex, openModal)
                    )}
                  </TableRow>
                ))}
              </TableBody>
            </Table>

            <Button
              variant="contained"
              color="primary"
              style={{ marginTop: "2rem" }}
              onClick={() => downloadUpdatedCSV(csvData, updatedData)}
            >
              Save and Download
            </Button>
          </ModalContainer>
        </Fade>
      </Modal>
    </AppContainer>
  );
});

export default Dashboard;
